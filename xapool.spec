Name:          xapool
Version:       1.5.0
Release:       15
Summary:       A series of open source code about XA JDBC Pool
License:       LGPLv2+
URL:           http://xapool.ow2.org/
Source0:       https://dllegacy.ow2.org/xapool/%{name}-%{version}-src.tgz
Source1:       https://repo.maven.apache.org/maven2/com/experlog/%{name}/%{version}/%{name}-%{version}.pom
Patch0000:     %{name}-%{version}-build.patch
Patch0001:     %{name}-%{version}-jdk7.patch
BuildRequires: ant apache-commons-logging geronimo-jta java-devel javapackages-local
BuildArch:     noarch

%description
XAPool is an XA database connection pool.XAPool implements javax.sql.XADataSource,
and provides connection pooling facilities. XAPool allows to pool objects, JDBC
connections and XA connections.

%package help
Summary:       Help documents for xapool package
Provides:      %{name}-javadoc = %{version}-%{release}
Obsoletes:     %{name}-javadoc < %{version}-%{release}

%description help
Help documents for xapool package.

%prep
%autosetup -n %{name}-%{version}-src -p1
find . -name "*.jar" -delete
find . -name "*.class" -delete
find . -name "*.java~" -delete

rm -rf $(find . -name "CVS")

sed -i "s|Class-Path: idb.jar classes12.jar jta-spec1_0_1.jar log4j.jar \
commons-logging.jar p6psy.jar||" archive/xapool.mf

ln -sf $(build-classpath commons-logging) externals/
ln -sf $(build-classpath geronimo-jta) externals/

rm -r src/org/enhydra/jdbc/instantdb src/org/enhydra/jdbc/oracle

%mvn_file com.experlog:%{name} %{name}

%build
ant dist

%install
%mvn_artifact %{SOURCE1} output/dist/lib/%{name}.jar
%mvn_install -J output/dist/jdoc

%files -f .mfiles

%files help -f .mfiles-javadoc
%doc README.txt

%changelog
* Fri Nov 18 2022 liyanan <liyanan32@h-partners.com>  - 1.5.0-15
- Change source

* Fri Dec 20 2019 wutao <wutao61@huawei.com> - 1.5.0-14
- Package init
